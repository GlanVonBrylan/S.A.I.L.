"use strict";

/* Pages manquantes :
 pixels
 Ancien Portail (ancient gateway)
 Précieux Trophées (treasured trophies)

 Orfraie le pingouin (Dreadwing; étymologie de effraie, les chouettes du genre Tyto, et peut être vu comme "celui qui fraie l'or", ce qui va avec sa nature de pirate)

 le Cultivateur
 Anciens
 monstres
 toutes les armes légendaires (voir l'Honorable arsenal de Biggy pour des traductions toutes faites)
 les bouffes
 PNJ
 vaisseaux de PNJ

 équipage

 colonies
 acte de colonie (colony deed) (redirige vers colonies)

 missions
 quêtes
 Arène
 Musée de Koïchi
 sacs de récompense
 salle de défi
 planètes, lunes, biomes etc
 anomalies (rencontres spatiales)
 type d'étoiles

 téléporteur
 enclume, station de câblage & co

 EPP, augments (en un seul article comme sur le wiki ?)
 bandages, kits médicaux etc dans la même page
 Techs
 lampe-torche
 torche
 fusée de détresse
 cannette de nourriture
 pioches et foreuses
 minerais

 agriculture (farming)
 griffe tranchante
 racine vivante
 figurines
 */

// Coups spéciaux des lances : flurry = déluge et blade whirl = tourbillon

// penguin est traduit "pingouin" au lieu de "manchot" pour plusieurs raison : c'est plus rigolo à dire, ça évite les pignouf qui demandent "ça devrait pas être pingouin ?" et c'est plus cohérent avec leur taille

const levenshtein = require('./levenshtein').levenshteinenator,

      aliases = Object.freeze({
          "manipulator module": "module de manipulateur",
          "upgrade module": "module d'amélioration",

          "console de navigation": "navigation", "navigation console": "navigation",
          carburant: "erchius", fuel: "erchius",

		  humain: "Humain·e·s", humains: "Humain·e·s",
		  avien: "Avien·ne·s", aviens: "Avien·ne·s",

          vep: "Apex", "processus vestigi-evo": "Apex", "vestigi-evo process": "Apex",
          kluex: "Avien·ne·s", aviolite: "Avien·ne·s",
		  greenfinger: "Mainverte",

          "terrene protectorate": "Protectorat Terrene", protector: "Protectorat Terrene", "protecteurice": "Protectorat Terrene", "protecteur": "Protectorat Terrene", "protectrice": "Protectorat Terrene",
          letheia: "Hylotls", "letheia corporation": "Hylotls",
          occasus: "culte Occasus",

		  tier: "rangs", tiers: "rangs",

		  outpost: "Avant-Poste",
		  "infinity express": "Infini Express",
		  terramart: "Terrafour",
		  "frögg furnishing": "Mobilier Frögg", "frogg furnishing": "Mobilier Frögg", "frog furnishing": "Mobilier Frögg",
		  EASEL: "CHEVALET", panneau: "CHEVALET", panneaux: "CHEVALET",
		  "2 stop teleshop": "Téléboutique 2-arrêts", "2-stop teleshop": "Téléboutique 2-arrêts",
		  "biggy's reputable weaponry": "Honorable Arsenal de Biggy",
		  "penguin bay": "Baie des Pingouins",
		  "penguin pete": "Pierre Pingouin", "auto chip": "Pierre Pingouin", "auto chips": "Pierre Pingouin", "puce auto": "Pierre Pingouin", "puces auto": "Pierre Pingouin",
		  "rob repairo": "Rob Réparo",
		  ark: "l'Arche",
		  baron: "Baron", // Sinon en tapant "baron" on voit l'Arche, car levensthein("baron", "ark") === 3

		  "terramart shipments": "livraisons Terrafour"
      }),

	  wiki = Object.freeze({

		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /********************************************************************** ESPÈCES **********************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  Humain·e·s: [
			  "https://i56.servimg.com/u/f56/17/10/54/90/humain10.png\nLes **Humain·ne·s** sont l’une des sept espèces jouables de Starbound, et l’une des six à s’être vue confier un Ancien Artefact.",
			  "La race humaine vient de la planète Terre et ressemble beaucoup à celle des __Apex__, hormis des sourcils moins saillants et une pilosité bien moindre ; il est dit que les Apex étaient probablement semblables aux Humain·ne·s avant que le PVE n’altère leur physiologie. Les Humain·e·s sont une espèce généralement inventive et optimiste, aiment explorer et apprendre, et souvent considérée inoffensive.",
			  "Nous en savons peu sur leur Histoire avant leur découverte du moteur supraluminique, bien qu’il est connu que cela impliquait une série de guerres et conflits pour des ressources limitées. La capacité de voyager rapidement entre les étoiles a rapidement mené à une société d’abondance, utilisant sa technologie avancée pour l’exploration et l’extension de leur savoir. Malgré leurs voyages intersidéraux, aucune colonisation à grande échelle n’a eu lieu, la plupart des Humain·e·s choisissant de revenir sur Terre ou de vivre en nomades de l’espace. Bien que leur colonies sont rares, les Humain·e·s sont souvent vu·e·s dans les avant-postes scientifiques et les routes spatiales à travers l’univers, incluant de nombreuses installations appartenant à la société Letheia des __Hylotls__.",
			  "Pendant un temps deux groupes majeurs se démarquaient : l’altruiste __Protectorat Terrene__, qui voulait aider et accueillir des êtres de toutes espèces pour le bien de toutes les vies de l’univers, et l’intéressée et moins droite Société Militaire Spatiale Universelle *(Universal Space Corporate Military*, ou *__U.S.C.M__.)*. Cette dernière a fini par s’effondrer après une série de mésaventures suivie par une catastrophe avec une arme biologique, et la plupart de ses membres se sont tourné·e·s vers la piraterie interstellaire.",
			  "L’humanité a reçu un coup terrible avec la destruction de la Terre par la __Ruine__. Des milliards de vie ont été perdues, ainsi que la plupart des membres du Protectorat qui étaient rassemblé·e·s à son Q.G. Cette catastrophe a été suivie par la montée du __culte Occasus__, une organisation sectète ultra-xénophobe d’Humain·e·s qui cherchent à exterminer toutes les autres espèces intelligentes à l’aide du pouvoir de la Ruine.",
			  "Malgré cela, l’humanité survit, et pourra peut-être à nouveau prospérer.",
			  "*Articles liés : Protectorat Terrene, USCM, culte Occasus, la Ruine*"
		  ],

		  Apex: [
			  "https://i56.servimg.com/u/f56/17/10/54/90/apex10.png\nLes **Apex** sont l’une des sept espèces jouables de Starbound, et l’une des six à s’être vue confier un Ancien Artefact.",
			  "Iels étaient à l’origine une espèce très proche de l’espèce __humaine__ jusqu’à ce qu’un mystérieux agent pathogène hydrique *totalement naturel* d’origine *inconnue* ne menace leur survie. *Heureusement* que leur gouvernement, connu sous le nom de __Miniknog__, a développé une cure contrant les effets néfastes du pathogène… avec un *effet secondaire inattendu* déclenchant le PVE (Processus Vestigi-Evo, ou *Vestigi-Evo Process (VEP)* en anglais) qui a « dévolué » leurs corps, les rendant semblables à des singes. D’un autre côté, cela a également amélioré leurs capacités cognitives, les menant vers des avancées plus grandes encore en super-science.",
			  "Ce processus a pris moins de dix ans pour s’étendre à l’espèce entière, et c’est *incidemment* au même moment que le Miniknog a entamé une ~~purge~~ restructuration sociétale. Toutes les traces de la société Apex précédant le VPE furent détruits, et __Big Ape__ (Grand Singe), dirigeant du Miniknog, prit le contrôle total et absolu de la société Apex… pour leur sûreté et leur sécurité, *bien sûr.* Le Miniknog dicte les emplois, l’économie, la mode, les rations de nourriture, et les « volontaires » pour les essais dans les avancements de la science biologique apex. Le règne de Big Ape est si omniprésent que personne ne sait depuis combien de temps la société apex est comme ça. Et ainsi les Apex sont *joyeuxes* et *complaisant·e·s*, sous le regard attentif de Big Ape et ses très, très, très nombreuses caméras de surveillances et gardes lourdement armés.",
			  "Pour *quelque raison obscure* cela a engendra un ressentiment parmi des Apex *ingrat·e·s et grossièr·e·s*, et des cellules rebelles sont apparues çà et là mais ce n’est *vraiment pas pas grand-chose*, et *absolument pas* une profonde tendance répandue dans toute la société.",
			  "Vaquez à vos occupation, citoyen·ne. Big Ape vous regarde.",
			  "*Articles liés : Miniknog*"
		  ],

		  Avien·ne·s: [
			  "https://i56.servimg.com/u/f56/17/10/54/90/aviens10.png\nLes **Avien·ne·s** sont l’une des sept espèces jouables de Starbound, et l’une des six à s’être vue confier un Ancien Artefact.",
			  "Espèce d’humanoïdes semblables à des oiseaux originaires de la planète Avos, dont les simples atours, la nature profondément religieuse et leurs tactiques de combat simplistes (consistant principalement à « submerger l’ennemi avec des milliers de soldats jetables ») les fait sembler primitif·ve·s à première vue. Pourtant, les Avien·ne·s sont très intelligent·e·s, et quiconque assez brave ou stupide pour s’aventurer dans un temple ou un tombeau avien trouvera des exemples de technologie avancée — et mortelle — alimentée par des cristaux d’avolite. Ces sources d’énergie énigmatiques ne sont extraites que par les prêtres Contemple-Étoiles sur leur planète natale, et alimentent tout, des simples lampes aux vaisseaux spatiaux capables de voyages intersidéraux.",
			  "La culture avienne tourne autour du culte de Kluex, le « dieu ailé de l’éther ». Selon leur religion, les Avien·ne·s vivaient autrefois hors du plan mortel aux côtés de leur dieu, mais sont devenu·e·s jalouxes des plaisirs mortels. Les Avien·ne·s sont entré·e·s dans le monde matériel, un acte qui fut puni en les privant de leur ailes éthérées, les condamnant à ne plus jamais voler. Un·e Avien·ne ne peut être réuni·e avec Kluex qu’en se défaisant de leur enveloppe charnelle et en gardant la plus fervente foi en son cœur. Des cérémonies ayant pour but d’atteindre cet état pur incluent le sacrifice rituel d’ennemi·e·s capturé·e·s, et une procession élaborée qui aboutit à lia plus pieuxe choisi·e par les prêtres grimpant une haute tour pour y faire un acte de foi en en sautant, pour que leur esprit se libère de leur prison mortelle et s’envole dans l’éther alors que leur corps abandonné s’écrase au sol.",
			  "Leur théologie sanglante n’est cependant pas du goût de toustes les Avien·ne·s, et un nombre croissant d’entre elleux rejettent les paroles des Contemple-Étoiles et la divinité de Kluex. Iels se nomment « Terrestres » *(grounded)*, et se sont lancé·e·s vers les étoiles pour former leurs propres colonies. Certain·ne·s se sont fabriqué des vaisseaux volants terrestres et vivent en pirates, et bien d’autres ont rejoint des organisations multiraciales telles que le __Protectorat Terrene__."
		  ],

		  Glitchs: [
			  "https://i56.servimg.com/u/f56/17/10/54/90/glitch10.png\nLes **Glitchs** sont l’une des sept espèces jouables de Starbound, et l’une des six à s’être vue confier un Ancien Artefact.",
			  "Bien qu’étant des robots extraordinairement avancés, leur société est littéralement médiévale. Fabriqués par un peuple inconnu il y a une éternité pour simuler la vie et la société organiques, *quelque chose* a mal tourné et les Glitchs sont pris·e·s dans une boucle infinie les empêchant de dépasser le stade des châteaux de pierre et villages agraires. Nous ne savons pas depuis combien de temps iels sont bloqué·e·s dans cet état ; bien des empires se sont élevés et sont tombés, et le peuple continue d’effectuer les tâches lui étant assignées jour après jour, satisfait de son travail, et laissant passer l’univers.",
			  "Les Glitchs iels-mêmes semble construit·e·s dans un âge sombre, leur apparence simpliste cachant la programmation extraordinairement poussée qui les fait fonctionner. Les chevaliers Glitchs ne sont guère construits d’un matériau plus solide qu’un simple cultivateur d’autotomates, donc les guerriers revêtent de lourdes armures en acier et portent d’anciennes armes de corps-à-corps, et parfois de simples arcs. Iels doivent manger pour se recharger, utilisant le méthane produit par la digestion comme carburant plutôt que la nourriture elle-même. Même leur « médecine » est censée simuler des soins organiques, bien qu’iels n’aient pas physiquement besoin qu’elle fonctionne de cette manière. Comme leurs « visages » ne sont pas conçus pour porter une expression, les Glitchs préfixent leur discours par une déclaration d’émotion d’un mot pour s’assurer que leur intention est claire. « Cordial. Heureux de vous rencontrer ! » « Enragé. Je vais vous fendre le crâne ! »",
			  "Les Glitchs meurent, et bien que le processus soit bien différent de celui des formes de vie biologiques, procréent. Les « parents » choisissent des pièces pour leur « progéniture », puis entrent dans une sorte de transe où iels assemblent le nouveau Glitch en silence et avec une vitesse et une habileté jamais vue dans les activités ordinaires des Glitchs. Ce processus est fait en plusieurs sessions réparties sur des semaines, et est effectué en privé.",
			  "Cependant, toustes les Glitchs ne restent pas enfermé·e·s dans leur programmation. C’est atypique, mais il arrive qu’un·e Glitch se libèrent des « angles morts » de leur code, et deviennent réellement conscient·e·s. Ces individus sont pris en pitié et pourtant honnis, l’autodétermination étant vue comme un défaut nuisant au mode de vie régulier des Glitchs. Ces « parias » conservent la plupart des particularités psychologiques de leur espèce malgré leur conscience, leur subit désir d’être *plus* que cela les amène à voyager entre les étoiles pour trouver le sens de leur vie.",
			  "Bien que la plupart des Glitchs restent dans leurs villages et leurs châteaux, quelques groupes dissidents se sont formés et répandus à travers le cosmos. Les Roublards Arc-en-ciel revêtent des capes et capuchons scintillants et colorés, apportant une justice éblouissante partout où ils vont. Moins vertueux, les Chevaliers Tout-Voyant — qui au moins ont un code d’honneur — ont soif de bataille et combattront pour quiconque leur offre un abri… contrairement aux Croisés Tout-Voyant au thème similaire, qui patrouillent les ruines des biomes d’yeux dans toute la galaxie et attaquent à vue."
		  ],

		  Floranes: [
			  "https://i56.servimg.com/u/f56/17/10/54/90/floran10.png\nLes **Floranes** sont l’une des sept espèces jouables de Starbound, et l’une des six à s’être vue confier un Ancien Artefact.",
			  "Elles sont des humanoïdes végétales carnivores et extrêmement violentes, n’ayant pas de gouvernement ni d’autorité centrale, étant divisées en de nombreuses tribus à travers l’univers, chacune étant dirigée par une __Mainverte__. Bien qu’étant une espèce unisexe, elle peuvent adopter ce qui ressemble à un dimorphisme sexuel selon leur volonté. Leur dialogue est souvent simple et elles ont tendance à sssiffler leurs consssonnes sssibilantes. Leur principale activité sont chasser, acquérir des trophées, manger de la viande, et encore chasser. D’aucuns spéculent que leur vision décontractée du fait de tuer et consommer d’autres espèces intelligentes vient d’une vision de la « viande » comme n’étant pas « vivante » de la même manière qu’elles le sont. Cependant, à mesure qu’elles se sont répandues à travers la galaxie, de plus en plus d’individus ont développé le besoin de s’intégrer, se faire des ami·e·s, et peut-être ne pas poignarder quiconque les importune.",
			  "Étant une espèce relativement jeune, leur technologie est primitive, mais elles ont un certain talent pour la récupération et — grâce aux dons uniques des Mainvertes — la rétro-ingénierie des technologies des autres espèces, au plus grand dam de l’univers.",
			  "Elles ont un passé assez moche avec les __Hylotls__, les deux espèces ayant été en guerre des années durant. Cela a abouti à une invasion massive des Floranes qui a poussé les Hylotls sous les océans. Bien que le conflit armé a cessé et que la paix dure depuis des générations, les deux espèces maintiennent un profond ressentiment mutuel. Les Hylotls font preuve d’un dédain et d’une condescendance passive-agressive à l’égard des Floranes, qui en retour insultent purement et simplement les Hylotls.",
			  "*Articles liés : Mainverte*"
		  ],

		  Mainverte: [
			  "**Mainverte** *(Greenfinger)* est le titre donné aux __Floranes__ spéciales qui agissent comme cheffes de leur tribu. Une tribu ne peut avoir qu’une Mainverte à la fois.",
			  "Malgré la culture brutale des Floranes, les Mainvertes sont rarement les plus fortes chasseuses de leur tribu. Elles sont plutôt davantage intelligentes (et patientes) que la Florane moyenne, et possèdent un don unique : la capacité de manipuler mentalement les végétaux. Mais surtout, elles peuvent le faire d’une manière qui permet aux plantes d’interagir avec des technologies avancées, rendant des machines complexes telles qu’un __téléporteur__ utilisables et reproductibles par les Floranes.",
			  "Grâce à ce don, même les Mainvertes jeunes et inexpérimentées sont révérées par le reste de la tribu, et leurs ordres sont suivis… quoique parfois à contrecœur, les Mainvertes ayant tendance à doucement pousser les Floranes bornées à être plus civilisées (ou au moins les détourner de leurs tendances les plus violentes), ce qui ne convient pas toujours à ces femmes-plantes impulsives.",
			  "*Article lié : Floranes*"
		  ],

		  Hylotls: [
			  "https://i56.servimg.com/u/f56/17/10/54/90/hylotl10.png\nLes **Hylotls** sont l’une des sept espèces jouables de Starbound, et l’une des six à s’être vue confier un Ancien Artefact.",
			  "Les Hylotls sont une espèce semi-aquatique avec une vision triloculaire, un génome très adaptable et une espérance de vie relativement courte, qui au fil des générations les ont mené·e·s d’une espèce strictement terrestre à une pouvant vivre confortablement en environnement aquatique, bien qu’iels n’aient toujours pas la capacité de respirer sous l’eau.",
			  "Iels ont un passé riche en arts, science et philosophie, avec une dévotion à la tradition et la méditation. Malgré leur niveau technologique très avancé, la plupart de leurs structures sont encore faites de bois orné de gravures. Cela mène à un conflit intéressant de cultures hylotls dans leurs cités sous-marines massives, où des calligraphies peintes à la main côtoient des panneaux publicitaires à néons pour la dernière mode, et où l’on trouve de tranquilles pièces-jardins tapissées de papier non loin de boîtes de nuit et leur « bubbstep » aux lancinantes et lourdes basses. La société Letheia, possédée par des Hylotls, est l’une des plus grandes sociétés de la galaxie, fournissant de tout, du simple divertissement aux stations spatiales en passant par le carburant pour vaisseaux.",
			  "Les Hylotls s’enorgueillissent de leur recherche de solutions pacifiques aux problèmes, bien qu’iels reconnaissent que parfois leur côté d’un débat sur les mérites de ne pas mourir a un peu plus de poids quand soutenu par une épée convenablement tranchante. Par conséquent, leur capacité à utiliser des armes avec talent et maîtrise est un art également perfectionné transmis au fil des générations.",
			  // "passif-agressif" est laissé au masculin parce que "passif·ve·s-agressif·ve·s" est tout bonnement illisible
			  "D’aucuns trouvent les Hylotls guindé·e·s, arrogant·e·s et passifs-agressifs, ce que les Hylotls nient, et iels s’imaginent que vous verrez votre erreur quand vous serez un peu plus éclairé·e comme iels le sont.",
			  "Les Hylotls ont un passé assez moche avec les __Floranes__. Bien que le pourquoi du comment a été perdu aux limbes de l’Histoire, les deux races ont été en guerre pendant longtemps. Les Hylotls s’en sont bien mal sorti·e·s malgré leur supériorité technologique, et une invasion Florane massive sur leur planète natale les a forcé·e·s à s’installer sous les océans. Bien que le conflit armé est terminé depuis bien longtemps, il y a toujours beaucoup d’animosité entre les deux espèces, bien que l’Hyloyl moyen affirmera qu’il n’y en a aucune et qu’iel a totalement pardonné aux Floranes d’être un ramassis de sauvages écervelées assoiffées de sang."
		  ],

		  Novakids: [
			  "https://i56.servimg.com/u/f56/17/10/54/90/novaki10.png\nLes **Novakids** sont l’une des sept espèces jouables de Starbound, et la seule à ne s’être pas vue confier un Ancien Artefact.",
			  "Sans doute l’une des espèces les plus mystérieuses, les **Novakids** sont des sacs de gas luminescents et conscient avec une forme humanoïde, avec un fer à la place du visage. Ce peuple généralement insouciant et capricieux, connu pour sa mémoire courte et une faible capacité d’attention, vit dans l’instant présent, vagabondant entre les étoiles à la recherche d’aventure ou quelque chose, avec peu d’intérêt donné au passé. Iels sont si indifférent·e·s à ce qui a pu se passer auparavant qu’iels ont oublié leurs propres origines, ne s’étant jamais préoccupé·e·s de marquer leur histoire. D’où iels viennent et comment se sont-iels formé·e·s restera peut-être à jamais un mystère.",
			  "Incrémentant l’étrangeté de leur existence, leur culture ressemble beaucoup à l’époque « Far West » de la Terre. Leur technologie repose grandement sur la vapeur, avec de longs vaisseaux spatiaux à wagons, et iels préfèrent de bons vieux revolvers aux armes de corps-à-corps. Que ceci a toujours été ou est récent est bien sûr inconnue, puisque les Novakids ne s’en souviennent sûrement pas.",
			  "Comme iels se sont toujours gardé·e·s à l’écart des affaires interstellaires, les Novakids assez dur·e·s à trouver dans l’univers."
		  ],


		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /******************************************************************* ORGANISATIONS *******************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  "Protectorat Terrene": [
			  "https://starbounder.org/mediawiki/images/6/61/Starbound2.jpg\nLe **Protectorat Terrene** est une coalition de plusieurs espèces pour entraîner des agents à protéger l’univers connu, créée par les __Humain·e·s__. Toustes les diplômé·e·s reçoivent un Manipulateur de Matière règlementaire qui leur permet et détruire et placer des matériaux. Le Protectorat est dirigé par un·e Grand·e Protecteur·rice.",
			  "Le Protectorat Terrene comprend sept espèces (__Humaine__, __Apex__, __Avienne__, __Florane__, __Glitch__, __Hylotl__ et __Novakid__), toutes jouables. Les membres de cette organisation sont appelé·e·s « Protecteur·rices ». Étant une organisation du maintien de la paix, le Protectorat est très connu dans l’univers de *Starbound*. Plusieurs __PNJ__ affirment avoir envisagé de rejoindre l’organisation, mais ne l’ont probablement pas fait à cause de l’incident arrivé au moment où *Starbound* se déroule.",
			  "L’Académie du Protectorat est un terrain d’entraînement élégamment décoré situé sur Terre dans une zone dense, comme le montre la silhouette des immeubles en fond. Les Ptrotecteurices recrues y sont entraîné·e·s avant d’obtenir leur diplôme. Ceci est fait lors d’une cérémonie où leur sera confié leur __Manipulateur de Matière__, symbole du Protectorat.",
			  "Dans *Starbound*, le Protectorat n’apparaît vraiment que dans la mission d’introduction. Lia joueur·se y est vu·e dans le dortoir de l’académie. S’ensuit la cérémonie de remise de diplôme à laquelle iel participe où la Grande Protectrice actuelle, Leda Portia, félicite les nouvelleaux diplômé·e·s. Alors que la cérémonie touche à sa fin, l’Académie est attaquée par un organisme inconnu qui détruit le bâtiment et tue la plupart des participant·e·s. La Grande Protectrice Leda Portia, sur le point de mourir, donne un Manipulateur de Matière à lia joueur·se et lui dit de prendre un __vaisseau__ pour fuir. Il est ensuite dit que le Protectorat ainsi que la planète Terre sont détruites.",
			  "*Articles liés : manipulateur de matière, la Ruine, Humain·e·s, Apex, Avien·ne·s, Glitchs, Floranes, Hylotls, Novakids*"
		  ],

		  "culte Occasus": [
			  "https://starbounder.org/mediawiki/images/8/88/Cultist_Set.png\nLe **culte Occasus** est le principal antagoniste dans *Starbound*. Dirigé par __Asra Nox__, ses fidèles sont des __Humain·e·s__ xénophobes qui cherchent à exterminer toutes les autres espèces intelligentes de l’univers. À cette fin, iels espèrent canaliser la puissance de __la Ruine__, malgré la menace qu’elle pose à *toute* vie, humaine ou autre.",
			  "Personne n’a dit qu’iels étaient futé·e·s. Nox elle-même pense qu’elle dirige un ramasiss d’abruti·e·s qui ne comprennent pas la vérité profonde de ses actions, mais que ce sont au moins des abruti·e·s *motivé·e·s* et *loyaux·ales*. Leur technologie est souvent dépassée, du niveau de celle des __Glitchs__, et même leurs vaisseaux sont décrépits et obsolètes.",
			  "Mais Nox est certaines que malgré cette drôle de bande, elle parviendra à ses fins.",
			  "*Articles liés : Asra Nox, la Ruine, Humain·ne·s, Protectorat Terrene*"
		  ],

		  Miniknog: [
			  "https://starbounder.org/mediawiki/images/4/40/Obey_Sign.png\nLe **Miniknog** est le gouvernement oppressif des __Apex__ ainsi que le principal antagoniste de la __mission__ *Allons chercher l’artefact apex*. Son nom est la contraction de *« Ministry of Knowledge »* (Ministère du Savoir).",
			  "Lia joueur·se découvre le Miniknog comme étant le pouvoir dominant les Installations d’essais apex, les Bases du Miniknog, les Villes d’appartements apex, et le Bastion du Miniknog. Son « dirigeant », __Big Ape__, est supposément un Apex divin et tout-puissant qui est constamment idolâtré par la propagande du Miniknog. Malgré ce que la plupart des Apex ont été poussé·e·s à croire tout leur vie durant, Big Ape est en réalité une imposture. La communauté Starbound ignore qui est réellement au sommet de la pyramide hiérarchique apex, mis à part le fait que c’est une oligarchie. Dans la mission du Bastion du Miniknog, lia joueur·se doit affronter une projection holographique de Big Ape en tant que boss.",
			  "Les Apex sont le seul peuple du jeu avec une guerre civile publiquement connue. Comme le Miniknog est un gouvernement brutal entièrement basé sur un immense tissu de mensonges et de propagande, il y a un mouvement universel, la Rébellion Apex, qui a pour but de renverser le Miniknog. Les rebelles anti-Miniknog peuvent être trouvé·e·s dans les Camps rebelles apex. Comparé aux tactiques et à l’armement hautement avancé du Miniknog — probablement le meilleur de l’univers connu — l’arsenal rebelle semble un poil primitif. Malgré ce désavantage, la résistance tient bon. __Lana Blake__ est l’une des figures les plus importantes du mouvement.",
			  "*Articles liés : Apex, Big Ape, Lana Blake*"
		  ],

		  USCM: [
			  "https://starbounder.org/mediawiki/images/1/19/USCM_Sign.png\nLa **Société Militaire Spatiale Universelle** *(Universal Space Corporate Military*, ou *U.S.C.M.)* était une organisation __humaine__ désirant « étendre la force et l’influence de l’humanité au-delà de la Terre ». Aux antipodes du __Protectorat Terrene__, l’USCM recherchait le profit en développant des armes et acquérant des ressources par la force. Son caractère impitoyable a mené à la création de nombreuses colonies pénales où les membres insubordonné·e·s étaient installé·e·s, avec la menace d’un emprisonnement planétaire censée garder le reste du personnel dans le rang.",
			  "Après plusieurs années de déficit financier, un nombre record de mutineries et de désertions et un sous-effectif dans les colonies pénales, l’USCM a signé un accord de développement d’arme biologique avec le __Miniknog__ (connu sous le nom de « projet __Parasprite__ ») dans l’espoir que cela retournerait la situation. Cependant, le Miniknog considérait l’USCM comme étant trop instable et dangereux, et a utilisé le projet pour détruire l’USCM de l’intérieur.",
			  "L’USCM a survécu, dans un sens : la plupart de ses colonies pénales demeurent, désormais dirigées par les prisonnièr·e·s, et leurs vaisseaux abandonnés dérivant dans l’espace. Aussi, il est dit qu’une bonne partie des bandits humains trouvés à travers l’univers sont d’anciens soldats de l’USCM qui utilisent leurs vieux vaisseaux et des technologies chapardées pour prendre une route vers le profit encore plus directe que celle de l’USCM.",
			  "*Articles liés : Humain·e·s, Protectorat Terrene, Miniknog, parasprite*"
		  ],


		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /*************************************************************** PERSONNAGES IMPORTANTS **************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  "Esther Bright": [
			  "https://starbounder.org/mediawiki/images/2/2e/Esther_Sprite.png\n**Esther Bright** est une __PNJ__ trouvée à l’__Arche__, près de la porte monolithique. Elle est la principale narratrice et source de quêtes pour l’histoire principale.",
			  "Il y a bien des années, elle recueillit une petite fille du nom d’__Asra Nox__, seule survivante du pillage d’une colonie par une bande composée de __Floranes__, d’__Apex__ et d’__Avien·ne·s__. Bien que s’épanouissant sous l’aile d’Esther, ce qui s’est passé à cette colonie ne fut jamais oublié par Asra qui développa une haine profonde des aliens. Esther finit par quitter le __Protectorat Terrene__, laissant son rôle de Grande Protectrice à Leda Portia, pour poursuivre ses recherches sur les __Anciens__. Elle finit par trouver de vieux écris parlant d’une « antique force de destruction à la puissance indicible » désirant détruire toute vie : __la Ruine__. Elle partagea cette découverte avec Asra, espérant profiter de son génie pour trouver un moyen de détruire la Ruine ; à la place, cela réveilla la haine d’Arsa, qui se mit à croire que la Ruine était une « force purificatrice » qui débarrasserait l’univers de toutes les « formes de vie inférieures ». Les tentatives d’Esther de la ramener à la raison ne firent que grandir sa colère, et Asra Nox quitta sa mentor pour fonder le __culte Occasus__, un groupe de suprémacistes humain·e·s entêté·e·s à l’idée de libérer la Ruine.",
			  "*Articles liés : Protectorat Terrene, Anciens, Asra Nox*"
		  ],

		  Nuru: [
			  "https://starbounder.org/mediawiki/images/thumb/2/2c/Nuru_character_art.png/300px-Nuru_character_art.png\n**Nuru** est la plus jeune championne de la grande chasse des __Grottes de chasse cérémonielles__ __Floranes__. Âgée de dix-sept étés, elle reste invaincue depuis plusieurs années. Elle est notablement plus intelligente que la Florane moyenne, et a été éduquée à être bien moins agressive à l’égard des autres formes de vie, ce qui l’a amnée à devenir curieuse, espiègle, et un poil sarcastique. Elle espère que les Floranes deviendront une espèce plus intelligente et acceptable socialement, tout en restant célèbre pour ses prouesses martiales.",
			  "Bien que plus petite et amicale que la plupart de ses semblables, Nuru est une guerrière mortelle, capable d’abattre des adversaires bien plus grands et s’élançant sans hésiter dans la mêlée avec un grand sourire.",
			  "Rencontrée en premier lieu durant la __mission__ *Allons chercher l’artefact florane*, elle y aide occasionnellement lia joueur·se à travers les grottes de chasse cérémonielles en lui ouvrant divers passages, puis affronte à ses côtés l’__Ixodoom__. Durant ce combat, elle est insensible aux attaques de l’Ixodoom et des __ixodeaux__.\nAprès ce combat, elle rejoindra l’__Avant-Poste__ où elle donnera la série de quêtes de l’__Arène__.",
			  "Par la suite, elle interviendra dans la mission *Allons chercher l’artefact glitch* aux côtés de __Lana Blake__ pour repousser les hordes d’adeptes __Occasus__ assaillant le donjon du __Baron__. Elle retournera avec Lana à l’Avant-Poste après que le Baron déclare la victoire, ce qui signifie qu’elle n’est plus là quand le __dragon squelette__ arrive.",
			  "**_Anecdotes_**\nQuand elle n’était qu’un bourgeon, elle a abattu un __oogler__ en à paine douze secondes!\nSon plat préféré est la __côtelette collante__, bien qu’elle aime également les autres viandes, ainsi que danser et faire des farces. Elle déteste le feu, les légumes, s’ennuyer et avoir faim.\nElle est présente dans *Wargroove*, un autre jeu de Chucklefish, en tant que commandante pour la faction « tribue floranes ».",
			  "*Articles liés : Floranes, l'Arène*"
		  ],

		  Koïchi: [
			  "https://starbounder.org/mediawiki/images/2/20/Koichi_character_art.png\n**Koïchi** est un jeune Hylotl le plus souvent trouvé les trois yeux rivés sur un livre. Il adore en apprendre autant que possible sur toutes les espèces de l’univers, cette soif de connaissance le pousse à explorer, l’amenant souvent à son insu dans des situations dangereuses, comme la fois où il était trop occupé à lire des hiéroglyphes pour remarquer que cet antique tombeau était bardé de pièges. Pour une personne aussi frêle et timide pour qui s’éclater consiste à faire un petit bac, c’est loin d’être idéal. Jusqu’ici, *quelqu’un* est toujours intervenu pour lui sauver la mise.",
			  "Il semble particulièrement troublé en la présence de __Nuru__, qu’il essaye d’impressionner à sa manière de geek.",
			  "Lia joueur·se le rencontre à la __Grande pagode-bibliothèque__ durant la __mission__ *Allons chercher l’artefact Hylotl*, où un groupe d’adeptes du __culte Occasus__ mené par __Asra Nox__ était Allons chercher l’artefact, forçant Koïchi à se barricader dans la salle la plus profonde, retenant les adeptes. Quand lia joueur·se arrive à la pagode, il utilise le réseau d’hologrammes de la bibliothèque pour lia guider, lui déverrouillant des portes pour lui permettre de progresser. Une fois Asra Nox vaincue, lia joueur·se rencontre Koïchi en face-à-face, et ce dernier décide de lia rejoindre et se rend donc dans un endroit plus sûr, l’__Avant-Poste__, où il donnera la série de quêtes du __Musée de Koïchi__, dédié aux diverses espèces du cosmos.",
			  "**_Anecdotes_**\nSa boisson préférée est le __récif-shake__, et il adore également faire des petits bacs, des quizzs, les anectodes historiques, et mettre des chaussettes avec ses sandales. Il déteste les réseaux sociaux et est allergique aux ananas.",
			  "*Articles liés : Hylotls, musée de Koïchi*"
		  ],

		  Tonuauc: [
			  "https://starbounder.org/mediawiki/images/thumb/4/4b/Tonauac_character_art.png/300px-Tonauac_character_art.png\n**Toauac** est un fervent adepte de Kluex, aussi pieux qu’on peut l’être. Bien qu’étant massif, il n’a rien d’un guerrier, et ce doux géant s’occupe à la place de garder propre et en état les parties publiques du __Grand Temple Souverain__… du moins les parties auxquelles il peut accéder.",
			  "Ne lâchant jamais son exemplaire de l’Avoscript, il propose gaiement aux visiteur·e·s un tour des salles non-léthales du temple tout en leur servant des histoires sur la grandeur et la magnificence de Kluex, la précision experte avec laquelle a été taillée cette table sacrificielle pour un flot de sang optimal, et s’il vous plaît ne touchez pas aux masques et ne vous approchez pas trop des scies circulaires en action. Oh et il vous rattrapera, juste après cette petite sieste pour se remettre de toutes ces émotions…",
			  "On le rencontre à l’entrée du Grand Temple Souverain, durant la __mission__ *Allons chercher l’artefact avien*. Il fait visiter à lia joueur·se quelques salles avant de devoir se reposer, s’endormant debout et donnant à lia joueur·se l’occasion de se faufiler vers la « série de défis et de pièges mortels » qui constitue tout le reste du temple. Après s’être réveillé, il retrouve lia joueur·se juste après la destruction de l’__Avatar de Kluex__, acte qui le convainc que lia joueur·se a été choisi·e par Kluex pour recevoir le trésor gardé par l’avatar, qui n’est autre que l’artefact avien. Après tout, seul·e un·e élu·e de Kluex pourrait vaincre son avatar, pas vrai?\nIl rejoint ensuite l’__Avant-Poste__, où il donnera à lia joueur·se l’un des __buffs__ suivants : régénération, éclat, saut amélioré, course améliorée, imuunité aux dégâts de chute, rage ou épines. Le buff change chaque jour.",
			  "**_Anectodes_** : Son plat préféré est le plat-plume, et ses principales activités consistent à vénérer Kluex, lire des livres, et dormir. Il déteste des chasseur·se·s de trésors et être appelé Tony.",
			  "*Articles liés : Avien·ne·s*"
		  ],

		  "Lana Blake": [
			  "https://starbounder.org/mediawiki/images/thumb/2/20/Lana_Blake_character_art.png/300px-Lana_Blake_character_art.png\n**Lana Blake** ne fait pas de quartier en combattant le __Miniknog__. Celle __Apex__ pragmatique et révolutionnaire est maîtresse en termes de tactique et gère le marché noir des camps rebelles, mais n’est pas pour autant du genre à rester en arrière. Elle n’hésite pas à mener la charge contre les installations lourdement armées du Miniknog, son écharpe volant au vent alors qu’elle arrose de plomb le champ de bataille. Elle est entièrement dévouée à renverses le gouvernement oppressif des Apex.",
			  "Lia joueur·se la rencontre en premier lieu au __Bastion du Miniknog__ alors qu’elle mène un commando de rebelles et demande à lia joueur·se — qu’elle prenait initialement pour un·e de ses soldats — d’aller dans les souterrains ouvrir diverses portes permettant à la force d’assaut principale de progresser. Vous finissez par vous rejoindre dans le bâtiment principal, mais Lana étant blessée, lia joueur·se doit poursuivre seul·e pour aller affronter « __Big Ape__ ».",
			  "Une fois ce dernier vaincu, Lana rejoint lia joueur·se dans les souterrains, où l’Ancien Artefact Apex est conservé. Elle se fiche éperdument de cette babiole et laisse lia joueur·se s’en emparer.\nElle rejoindra ensuire l’__Avant-Poste__, où elle vendra des __augments__ pour le __PPE__, son stock changeant tous les jours.",
			  "Par la suite, elle interviendra dans la __mission__ *Allons chercher l’artefact glitch* aux côtés de __Nuru__ pour repousser les hordes d’adeptes __Occasus__ assaillant le donjon du __Baron__. Elle retournera avec Nuru à l’Avant-Poste après que le Baron déclare la victoire, ce qui signifie qu’elle n’est plus là quand le __dragon squelette__ arrive.",
			  "**_Anectodes_**\nContrairement à la quasi-totalité des Apex, elle ne digère pas les bananes ; son plat préféré est le __donut flippant__. Elle aime aussi les écharpes et vandaliser les propriétés du Miniknog.",
			  "*Articles liés : Apex, Miniknog*"
		  ],

		  Baron: [
			  "https://starbounder.org/mediawiki/images/thumb/9/99/Baron_character_art.png/300px-Baron_character_art.png\nLe **Baron** (aussi connu sous les noms « Baron 4c 69 67 68 74 » et « Baron Lumière ») est un __Glitch__ aux milles histoires d’aventures à la véracité douteuse. Cet excentrique notable parcours l’univers pour corriger le mal (et parfois aussi le bien par accident) selon son humeur. Sa carrière est longue, glorieuse et controversée, pouvant tout aussi bien être acclamé qu’injurié selon l’endroit.",
			  "Le Baron profite présentement de sa retraite, assis sur sa collection de trésors. Mais peut-être qu’un personnage convenablement aventureux pourrait l’emmener sur une dernière quête…",
			  "Lia joueur·se le rencontre dans le __Donjon du Baron__ durant la __mission__ *À la recherche de l’arteface glitch*, où il se trouve en possession dudit artefact. Il accueille chaleureusement lia joueur·se mais refuse de se séparer de son joyau. C’est alors que son château est attaqué par le __culte Occasus__, ce qui n’était visiblement pas la première fois. Lassé de ces assauts, il propose à lia joueur·se de lui donner l’artefact s’iel l’aide à défendre son domaine. (assaut durant lequel lui ne bougera pas le petit doigt)",
			  "Des dizaines d’adeptes assaillent alors la muraille, à l’aide d’unités volantes (les *hélicultistes*) et de catapultes. __Nuru__ et __Lana Blake__ arrivent durant le combat pour aider, et repartent une fois l’assaut repoussé. Malheureusement, une deuxième vague d’ennemis vient ensuite, appuyée par __Asra Nox__ chevauchant un immense __dragon squelette__.",
			  "Une fois le dragon vaincu, le Baron cède l’artefact, puis rejoins l’__Avant-Poste__. Il y donnera la quête __La forge des héros__, permettant d’améliorer l’__épée brisée__ en __lame du Protecteur__, le seul équipement de rang 7.",
			  "**_Anecdotes_**\nSon plat préféré est le __friand rouillé__. Il aime également se vanter de ses exploits et amasser des babioles brillantes. Il est l’ennemi juré du Compte 01 et de l’ancien et puissant RAMpire.",
			  "*Articles liés : Glitchs, épée brisée, lame du Protecteur*"
		  ],

		  "Hiraki Corale": [
			  "https://starbounder.org/mediawiki/images/0/06/HirakiCorale-art.png\n**Hiraki Corale** est une aventurière __Hylotl__, célèbre pour sa bravoure, sa ténacité, son attitude volontaire et enjouée, et sa stupéfiante inconscience des ennuis qu’elle cause. Elle explore l’univers pour en apprendre sur les autres espèces et leur culture, et les notes de ses voyages se sont répandues un peu partout.",
			  "Elle a été pourchassée par des __Floranes__ en colère, chassée d’un dirigeable __avien__, pourchassée par des __Glitchs__ en colère, chassée par le __Miniknog__… Il est difficile de trouver une espèce dont elle n’a pas tapé les nerf d’une manière ou d’une autre et dont elle n’a pas reçu de volée de bois vert. Heureusement, c’est une athlète hors pair parée à toute éventualité, et est tout simplement extrêmement chanceuse.",
			  "Sont arme favorite est le lance-roquettes. Lia joueur·se ne peut pas rencontre Hiraki en personne, seulement la découvrir à travers ses __codex__ trouvés aléatoirement dans les villages et donjons.",
			  "*Article lié : Hylotls*"
		  ],

		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /********************************************************************** OUTILS ***********************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  "manipulateur de matière": [
			  "https://starbounder.org/mediawiki/images/e/ed/Matter-Manipulator.png\nLe **Manipulateur de Matière** *(matter manipulator)* est le prmier outil obtenu par lia joueur·se, et ce durant la mission d’introduction où la Grande Protectrice le lui confie avant d’être entraînée sous le sol par un tentacule. Le Manipulateur peut être amélioré au fil du jeu à l’aide de __Modules de Manipulateur__, et reste tout du long le principal outil de minage. Le raccourci par défaut pour l’équiper est \"R\". Des fonctions supplémentaires peuvent être débloquées, comme le mode de câblage ou celui de peinture.",
			  "Au début, le Manipulteur fait 1,2 dégâts de bloc 4 fois par seconde sur un carré de 2×2, à une distance maximale de 14 cases. Chaque amélioration du générateur d’énergie *(power generator)* augmente les dégâts de bloc de 1,2, et chaque amélioration de l’unité de traitement de matière *(matter proc unit)* augmente la largeur et la hauteur de la zone de minage de 1.",
			  "Le Manipulateur peut être amélioré 12 fois, offrant la possibilité de récolter les liquides, augmentant la puissance de minage, la taille de la zone de minage et augmentant sa portée. C’est le seul outil permettant de récolter des liquides.",
			  "Amélioré au maximum, c’est le meilleur outil de minage du jeu, permettant de creuser une zone de 5×5 avec une puissance de minage de 19,2/s. Cependant, si vous avez seulement besoin de creuser vers le bas un tunnel vertical large de 2, une perceuse en diamant est plus rapide (35 dégâts de bloc par seconde).",
			  "Il existe en existe une version impossible à obtenir, le Manipulateur de Matière X, qui est en tous points semblable à un Manipulateur de Matière normal amélioré à fond, à ceci près qu’il fait des bruits de pioche et que son faisceau a une apparence différente.",
			  "**     __Améliorations__**\n**Port d’extensions** *(expansion slot)*\n1 : Permet de récolter les liquides. Coût : 3 modules.\n2 : Débloque le mode peinture. Coût : 3.\n3 : Débloque le mode câblage. Coût : 3.\n**Unité de traitement de matière** *(matter proc unit)*\n1 : Passe la taille de lazone de minage à 3×3. Coût : 12.\n2 : Passe la zone de minage à 4×4. Coût : 24.\n3 : Passe la zone de minage à 5×5. Coût : 48.\n**Générateur d’énergie** *(power generator)*\n1 : Double la vitesse de minage. Coût : 8.\n2 : Triple la vitesse de minage. Coût : 16.\n3 : Quadruple la vitesse de minage. Coût : 32.\n**Lunette** *(optics)*\n1 : Augmente la portée du manipulateur de 2 cases. Coût : 6.\n2 : Augmente la portée du manipulateur d’encore 2 cases. Coût : 12.\n3 : Augmente la portée du manipulateur d’encore 2 cases. Coût : 24.\n*Note : la portée du manipulateur est toujours la même pour poser des blocs que pour les détruire.*\n**Coût total : 191 modules de manipulateur**",
			  "*Articles liés : mode peinture, mode câblage, module de manipulateur*"
		  ],

		  "mode peinture": [
			  "https://starbounder.org/mediawiki/images/1/1f/Paint_Mode.png\nLe **mode peinture** du __Manipulateur de Matière__ permet à lia joueur·se de peindre la plupart des blocs. Il a un emplacement permanent dans la barre d’inventaire et le raccourci par défaut pour l’équiper est \"Y\". Le Manipulateur de Matière doit être amélioré pour débloquer ce mode.",
			  "Une fois équipé, le clic droit permet de choisir la couleur entre (dans l’ordre) rouge, bleu, vert, jaune, orange, rose, noir, blanc et aucune. Le clic gauche permet de peindre (ou, si vous avez sélectionné l’absence de couleur, de nettoyer). La zone de peinture est la même que pour miner, et peut également être restreinte à un seul bloc en appuyant sur Shift.",
			  "*Articles liés : manipulateur de matière, mode câblage*"
		  ],

		  "mode câblage": [
			  "https://i56.servimg.com/u/f56/17/10/54/90/czebla10.png\nLe **mode câblage** du __Manipulateur de Matière__ permet de câbler des objets entre eux, déclenchant des actions automatiquement. Il a un emplacement permanent dans la barre d’inventaire et le raccourci par défaut pour l’équiper est \"T\". Le Manipulateur de Matière doit être amélioré pour débloquer ce mode.",
			  "Quand il est actif, les ports d’entrée (marqués par des cercles bleus) peuvent être connectés à des ports de sortie (cercles rouges), ou déconnectés (sauf dans les zones protégées). Pour connecter deux ports, faites clic gauche sur l’un, puis clic gauche sur l’autre. Pour détruire toutes les connexions d’un port, faites clic droit dessus.",
			  "Quand un objet est activé (par exemple quand vous appuyez sur un bouton, ou ouvrez une porte), son port de sortie émet un signal. Les objets dont le porte d’entrée reçoit un signal sont activés (allumant un lampe ou ouvrant une porte, par exemple). Quand le signal cesse, l’objet est désactivé (la lampe s’éteint ou la porte se referme). Les objets sont le port d’entrée est connecté ne peuvent plus être actionnés manuellement.",
			  "La __station de câblage__ *(wiring station)* permet de fabriquer tout un tas d’éléments complétant la macénique de câblage, comme des détecteurs, boutons, leviers ou portes logiques.",
			  "**Boutons, leviers et détecteurs**\nLes leviers, consoles et terminaux ont deux états, activé et désactivé. En état activé, leur port de sortie émet un signal en permanence.\nLes boutons déclenchent un signal de sortie pendant une seconde après avoir été activés.\nLes détecteurs émettent un signal de sortie tant qu’une condition est remplie, comme le fait d’avoir un personnage près d’un détecteur de proximité *(proximity scanner)* ou qui se tient sur une plaque de pression *(big red button, small floor button)*. Il existe aussi un détecteur de lumière *(light sensor)* et un de liquides *(liquid sensor)*.",
			  "**Portes logiques**\nVerrou *(latch)* : Sa sortie est dans le même état (activé ou désactivé) que son premier port d’entrée, tant que le second port d’entrée est activé. Si le second port d’entrée est désactivé, le verrou gardera son état de sortie (allumé ou éteint) quel que soit l’état du premier port d’entrée.\nPorte NON *(NOT switch)* : Sa sortie est activée si son entrée est désactivée, et vice-versa.\nPorte OU *(OR switch)* : Sa sortie est activée tant qu’au moins une de ses entrées l’est.\nPorte ET *(AND switch)* : Sa sortie est activée dans que ses deux entrées le sont.\nPorte OU EXCLUSIF *(XOR switch)* : Sa sortie est activée tant qu’une seule de ses entrées est activée.\nMinuteur *(timer)* : Sa sortie change d’état toutes les demi-secondes tant que son entrée est désactivée. Quand son entrée est activée, sa sortie conserve son état.\nCompte à rebourd *(countdown timer)* : Quand son entrée est activée, sa sortie reste activée pour une durée comprise entre 1 et 5 secondes, puis il se désactive. La durée peut être ajustée en interagissant avec (E).\nDélai *(delay gate)* : Quand son entrée est activée, sa sortie s’active après une attente d’une demi-seconde.",
			  "*Articles liés : manipulateur de manière, station de câblage, mode peinture*"
		  ],

		  "module de manipulateur": [
			  "https://starbounder.org/mediawiki/images/1/1d/Manipulator_Module_Icon.png\nLes **modules de manipulateur** *(manipulator module)* sont des objets spéciaux permettant d’améliorer le __Manipulateur de Matière__. On en trouve dans les coffres et les __sacs de récompense__ *(reward bags)*. Un total de 191 de ces modules est requis pour améliorer le Manipulateur au maximum.",
			  "*Article lié : manipulateur de matière*"
		  ],

		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /******************************************************************** ÉQUIPEMENTS ********************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /********************************************************************* APPAREILS *********************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  "livraisons Terrafour": [
			  "https://starbounder.org/mediawiki/images/5/58/Terramart_Shipments.gif\n**Livraisons Terrafour** *(Terramart shipments)* est un « marchand » portable qui permet de vendre les graines, la nourriture les biens obtenus via les bourbisons *(fluffalos)*, butins de monstres (__griffes tranchantes__, __racines vivantes__, etc), textiles et tissus à 30 % de leur prix, au lieu des 20 % habituels. Les autres types de biens (comme les équipements, outils, meubles...) peuvent être transférés mais aucun __pixel__ ne sera reçu en retour.",
			  "La cargaison étant propulsée dans l’espace, l’installation ne doit pas être couverte pour pouvoir fonctionner.",
			  "*Articles liés : Terrafour, agriculture*"
		  ],


		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /***************************************************************** MÉCANIQUES DE JEU *****************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  rangs: [
			  "Les **rangs** *(tiers)* définissent le niveau d’avancement des joueur·se·s. Distribués de 1 à 8, il définissent la puissance des __armes__, __armures__, __PNJ__, le niveau des appareils (__enclumes__ etc), des __minerais__, la qualité des marchands et trésors, et la dangerosité des __boss__, __planètes__, __vaisseaux de PNJ__ et __anomalies__.\nPour une liste complète du contenu des différents rangs, voyez https://starbounder.org/Tier (en anglais).",
			  "*Articles liés : tous les trucs soulignés. Sans déconner, les rangs sont omniprésents, tout y est lié.*"
		  ],

		  navigation: [
			  "https://starbounder.org/mediawiki/images/thumb/4/49/Star_map.png/750px-Star_map.png\nLa **navigation** entre les __planètes__ et __étoiles__ est faite depuis la console de navigation, accessible en vous asseyant sur la chaise du capitaine dans votre __vaisseau__. Celle-ci vous affiche une carte de l’espace, dans laquelle vous définissez votre destination en faisant un clic droit. Votre destination peut être définie :\n – En sélectionnant une planète ou une lune ;\n – En entrant des coordonnées ;\n – En cliquant sur « Afficher le lieu de quête » *(\"show quest location\")* ;\n – En sélectionnant un marque-page.\nCes trois dernières options sont disponibles via le boutons en haut à gauche de la carte, de même que le bouton « afficher l’emplacement du vaisseau » *(\"show ship location\")*, qui permet à la carte de se centrer sur votre vaisseau.",
			  "En faisant clic gauche sur une __étoile__ dans la carte des étoiles, vous pouvez afficher le type de l’étoile, le nom de son système, et les types de planètes qui l’orbitent. De même, dans la carte d’un système solaire, vous pouvez faire clic gauche sur une planète ou une __lune__ pour en afficher les détails (type, dangerosité, minerais présents et météo), à condition d’avoir déjà orbité la planète, le vaisseau ou la station en question. Il en est de même pour les __vaisseaux de PNJ__ et les __stations orbitales__.",
			  "Dans la carte d’un système solaire, double-cliquer sur une planète zoome dessus, et cliquer sur le bord de la carte vous recule à la carte des étoiles. Dans cette dernière, double-cliquer sur une étoile vous affichera la carte de son système si vous l’aviez déjà visité.",
			  "**     __Marque-pages__**\nQuand vous affichez les détails d’une planète, lune ou station orbitale, vous pouvez cliquer sur le bouton « ajouter un marque-page » *(\"add bookmark\")* pour la mettre en marque page, et pouvoir ainsi retrouver facilement ce lieu.",
			  "**     __Carburant__**\nVoyager au sein d’un système solaire ne coûte rien d’autre qu’un peu de temps, mais voyager d’une étoile à une autre vous coûtera 100 unités de __carburant__ (ou moins si vous avez un·e ou plusieurs mécanicien·ne·s ; voir __équipage_₎. Votre jauge de carburant se trouve à l’entrée de la cabine de pilotage. Elle contient six emplacements d’inventaire : mettez-y de l’erchius liquide ou en cristaux puis cliquez sur « alimenter » *(\"fuel\")*.",
			  "*Articles liés : vaisseau, étoiles, planètes, lunes, carburant*"
		  ],

		  véhicules: [
			  "Les **véhicules** permettent au joueur·se·s de se déplacer rapidement. Ils ont un siège conducteur et un siège passager — à noter que lia conducteurice ne peut que conduire, et que lia passagèr·e ne peut riien faire du tout. Dans l’inventaire, ils se présentent sous la forme de « pokéballs » : clic gauche permet de déployer un véhicule, et clic droit sur le véhicule en tenant sa « ball » le ramène dedans. Si vous quittez une planète alors que votre véhicule est dessus, il est détruit. Les véhicules sont vendus par __Pierre Pingouin__ et réparés auprès de __Rob Réparo__.",
			  "**Hoverbikes** https://starbounder.org/mediawiki/images/1/12/Red_Hoverbike.png\nLittérallement « moto flottante », ils sont faits pour se déplacer très rapidement sur terre (jusqu’à 80 cases par seconde, contre les 14 des joueur·se·s), et flottent à environ 3 cases du sol, leur permettant d’assez facilement gravir les pentes. Quand vous le conduisez, clic gauche allume ou étteint les phares et clic droits donne un coup de klaxon. Ils sont disponibles en trois coloris différents (kaki, rouge et vert).\nIls ont 1 000 PV et une résistance aux dégâts de 50 %. Lia conducteurice a une résistance de 85 % (qui remplace celle de son armure) et lia passagèr·e une résistance de 100 %. L’hoverbike perd des PV en se cognant sur le terrain ou en étant attaque par des monstres, et fumera de plus en plus à mesure qu’il est endommagé, jusqu’à exploser.\nIl est à noter que les hoverbikes se déplacent très bien dans les fonds marins.",
			  "**Bateaux** https://starbounder.org/mediawiki/images/7/71/Boat_2.png\nLes bateaux permettent de se déplacer rapidement (35 cases par seconde) à la surface de n’importe quel liquide, même la lave, bien que semblant être fait de bois. Le bateau sert de plateforme, permettant à des joueur·se·s de s’y tenir et de voyager dessus sans être bloqué dans le siège conducteur passager, ce qui le permet par exemple de se battre mais augmente leur risque de choir.\nLes navires ont une résistance aux dégâts de 50 % mais n’ont que 100 PV et auront donc une force tendance à se désintégrer, surtout si vous rentrez dans un rivage à toute vitesse.",
			  "*Articles liés : Pierre Pingouin, Rob réparo, mécas*"
		  ],


		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /*********************************************************************** LIEUX ***********************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  vaisseau: [
			  "*Cette page concerne les vaisseaux des joueur·se·s. Il y a un article séparé pour les __vaisseaux de PNJ__.*",
			  "https://starbounder.org/mediawiki/images/1/1e/Ships.gif\nDans *Starbound*, voyager de planète en planète est fait à l’aide de **vaisseaux** interstellaires. Ces vaisseaux sont différents pour chaque espèce ; il y a ainsi un total de sept vaisseaux différents.",
			  "Si le vaisseau est le vôtre, vous asseoir dans la chaise de lia capitaine ouvrira la console de __navigation__. Après avoir réparé votre vaisseau, vous pourrez le recharger en carburant (de l’__erchius_₎ pour pouvoir activer son moteur supraluminique qui vous permet de voyager d’étoile en étoile. Vous y trouverez également __S.A.I.L.__, votre ordinateur de bord.",
			  "Équipé dès le départ d’un __téléporteur__ et d’un casier de rangement indestructibles, votre vaisseau peut vous servir de base d’opérations, qui aurait en plus l’avantage de vous suivre de planète en planète. La personnalisation du vaisseau est partie intégrante du jeu, les lumières, portes et murs de fond pouvant être détruits et replacés à volonté. Le casier du vaisseau est le plus grand meuble de rangement du jeu, ayant 64 emplacements, et contient de base une __lampe-torche__, 10 __torches__ et 4 __cannettes de nourriture__.",
			  "Dans votre vaisseau se trouve également votre __animal de compagnie__ ainsi que votre __équipage__. Le nombre de membres d’équipage que vous pouvez avoir commence à 2, et augmente de 2 à chaque agrandissement du vaisseau. Celui-ci pouvant être agrandi cinq fois, la taille maximum de votre équipage est donc de 12. D’ailleurs, atteindre la limite de membres d’équipage vous permet de recevoir une licence pour un vaisseau plus grand, vous débloquant une quête permettant de donner des __modules d’amélioration__ *(upgrade modules)* à __Pierre Pingouin__, à l’__Avant-Poste__, pour qu’il agrandisse votre vaisseau. Le premier agrandissement coûte 2 module, et chaque autre en coûte encore 2 de plus, pour un total de 30 modules pour agrandir votre vaisseau au maximum. Les classes de vaisseaux portent, dans l’ordre, les noms de frégate, corvette de classe moineau, corvette de classe crécerelle, croiseur de classe faucon, croiseur de classe aigle, et cuirassé de classe condor. Si vous ne voulez pas vous embêter à recruter des membres d’équipage, vous pouvez aussi acheter des licences contrefaites à la __Baie des Pingouins__… si vous avez assez de __pixels__.",
			  "*Articles liés : navigation, équipage, animal de compagnie, S.A.I.L., Baie des Pingouins, Pierre Pingouin*"
		  ],

		  "module d'amélioration": [
			  "https://starbounder.org/mediawiki/images/b/bd/Upgrade_Module_Icon.png\nLes **modules d’amélioration** *(upgrade modules)* sont des objets spéciaux utilisés pour améliorer certaines armes et pour agrandir votre __vaisseau__. On peut en trouver dans n’importe quel coffre, mais ils sont plus fréquents dans les __salles de défi__ *(challenge rooms)*.",
			  "Le premier agrandissement de vaisseau requiert 2 de ces modules, et chaque agrandissement supplémentaire en requiert chaque fois 2 de plus, pour un total de 30 modules requis pour l’agrandir au maximum.",
			  "Les armes suivantes peuvent être fabriquées à l’__enclume__ ou à __l’Honorable armurerie de Biggy__ *(Biggy’s reputable weaponry)* et requièrent chacune 1 module (entre autres composants) : __gant à griffe__ *(claw glove)*, __gant taser__ *(stun glove)*, __gant-vigne__ *(vine fist)*, __boomerang en fusion__ *(molten boomerang)*, __chakram renforcé__ *(hard chakram)*, __néo chakram__, __chakram-scie__ *(saw chakram)*, __boomerang gelé__ *(frozen boomerang)*, __lunarang__.",
			  "*Articles liés : vaisseau, l’honorable armurerie de Biggy*"
		  ],

		  SAIL: [
			  "https://cdn.discordapp.com/avatars/484427585116176395/c65b508c9017ab35f68750505891b69b.png\n**S.A.I.L.**, acronyme pour *Ship-based Artificial Intelligence Lattice* (matrice d’intelligence artificielle de __vaisseau__) est l’intelligence artificielle consciente qui contrôle l’ordinateur de bord de lia joueur·se. S.A.I.L. se présente sous une forme humanoïde sans grand détail. La console sur laquelle elle est accessible permet à lia joueur·se d’embarquer sur une des __missions__ qu’iel a débloquées et de passer en revue son __équipage__.",
			  "De temps en temps, S.A.I.L. fera des commentaires sur votre progression dans le jeu, d’ordinaire durant les missions de l’histoire principale, avec Esther. Les commentaires de S.A.I.L. sont en général des informations utiles suivies d’un compliment inexpressif, par exemple « Cette planète est habitée par de nombreux êtres hostiles. J’applaudis votre bravoure démontrée par l’intérêt que vous portez à ce monde. »",
			  "Quand lia joueur·se visite une planète dangereuse sans l’équipement de protection adéquat, S.A.I.L. s’empressera de réciter un avertissement générique vous demandant de ne pas poursuivre.",
			  "La couleur de S.A.I.L. et l’apparence de sa console dépendent de l’espèce de lia joueur·se. Durant la bêta, chaque espèce avait une apparence différente : __Big Ape__ pour les __Apex__, un vieux sorcier pour les __Glitchs__, un amas de cristaux d’aviolite pour les __Avien·ne·s__, un vénérable pour les __Hylotls__, et un cheval pour les __Novakids__. Celle des __Humain·e·s__ avait l’apparence que l’on retrouve ajourd’hui, de même que les __Floranes__, au détail près que l’IA de cette dernière avec des spirales à la place des yeux",
			  "C’est la console de S.A.I.L. qui fait apparaître votre __animal de compagnie__. Utiliser des commandes pour faire apparaître une console de S.A.I.L. fera apparaître l’animal, mais en faire apparaître une seconde dans le même monde fera planter le jeu, ce dernier ne sachant pas comment gérer deux animaux de compagnie à la fois. (dissidence!)",
			  "*Articles liés : vaisseaux, missions, animaux de compagnie*"
		  ],


		  "Avant-Poste": [
			  "L’**Avant-poste** *(Outpost)* est une structure accessible via les __Anciens Portails__ présents sur la planète de départ de lia joueur·se ou sur les autres planètes luxuriantes. Il est peuplé de __PNJ__ de toutes espèces et contient divers magasins.",
			  "Lia joueur·se y arrive d’abord à un Ancien Portail inutilisable, et trouvera l’Avant-Poste en se dirigeant vers la droite. Par la suite, iel pourra y retourner par téléportation en enregistrant le téléporteur près de la __Téléboutique 2-stop__.",
			  "Hormis l’__Infini Express__ et __Marcel Manchot__, tous les magasins commencent fermés et s’ouvrent à mesure que l’on progresse dans l’histoire. __Mobilier Frögg__, la __Téléboutique 2-arrêts__ et __Terrafour__ sont débloqués en finissant la __mission__ du *Complexe de minage d’Erchius*. __CHEVALET__ et la __Baie des Manchots__ le sont en finissant *Allons chercher l’artefact florane*. Les __Précieux Trophées__ et l’__Honorable Arsenal de Biggy__ apparaissent en terminant *Allons cherche l’artefact hylotl*, mais ce dernier n’offrira ses services qu’en terminant la mission secondaire du *Site d’essais d’armes des pingouins*. __Ursa Mineur__ sera ouvert en finissant la __quête__  *l’ourson perdu*.",
			  "On y trouve également le labo __Tech__, à côté duquel se trouvent une console d’équipement de Techs et un distributeur permettant d’en acheter. En sous-sol il y a l’Honorable Arsenal de Biggy, le bar __Aisébec__, l’__atelier à mécas__ et __Mazebound64__.\nLes magasins sont liés à l’univers, ce qui signifie qu’un·e joueur·se n’ayant fait aucune quête aura tout de même accès aux échoppes débloquées par les autres joueur·se·s jouant dans le même univers.",
			  "**Carte : https://starbounder.org/mediawiki/images/4/4e/Outpost_Map.png**\n**Légende :** Numéros jaunes : quêtes. Lettres oranges : lieux notables. Lettres bleues : magasins.**1** : Labo tech, **2** : Ursa Mineur, **3** : Pierre Pingouin\n**A** : cafétéria, **B** : console d’équipement de Techs, **C** : téléporteur, **D** : Rob Réparo\n**a** : Infini Express, **b** : CHEVALET, **c** : Mobilier Frögg, **d** : Terrafour, **e** : distributeur de consoles Tech, **f** : Ursa Mineur, **g** : Téléboutique 2-arrêts, **h** : baie des pingouins, **i** : Pierre Pingouin.",
			  "N’apparaissent pas sur cette carte : le sous-sol (en dessous), Précieux Trophées (à droite), et le vendeur de clefs anciennes (juste au-dessus de la téléboutique).\nSpoiler : ||Sous l’astéroïdes de l’Avant-Poste se trouve une pièce secrète où l’on peut obtenir un masque. Elle est accessible en longeant le bord tout à gauche avec des grappins ou la boule à pics.||",
			  "*Article liés : l’Arche*"
		  ],

		  "Infini Express": [
			 "https://starbounder.org/mediawiki/images/1/1d/Infinity_Express.png\nL’**Infini Express** *(Infinity Express)* est une supérette de l’__Avant-Poste__. On y trouve __lampe-torches__ (500p), __bandages__ (125p), __kits médicaux__ (300p), __fusées de détresse__ (25p), cordes (50p), __Erchius__ liquide (5p), piles LR6 *(AA battery)* (250p), batteries (2500p), tissu *(woven frabric)* (50p), cuir (250p), soie (500p), toile *(canvas)* (750p), matériau synthétique (1000p), bouteilles vides (50p), __cannettes de nourriture__ (200p), fromage (30p), chocolat (20p), œufs (20p), lait (20p), bacon cru (20p), pommes (20p), soda (20p).",
			  "*Articles liés : Avant-Poste, pixels*"
		  ],
		  Terrafour: [
			  "https://playstarbound.com/wp-content/uploads/2014/06/terramartFLORAN.gif\n**Terrafour** (*Terramart*, jeu de mots avec Walmart, l’équivalent de Carrefour aux États-Unis) est un magasin de l’__Avant-Poste__ spécialisé dans l’agriculture et les animaux de compagnie.",
			  "**Inventaire :**\n__livraisons Terrafour__ (3000p)",
			  "Divers œufs à poser pour obtenir des animaux : de poule (1000p) pour avoir… des œufs (à manger seulement)\nDe poule robot (1000p) pour avoir des piles LR6 *(AA battery)*\nDe meuhshi *(mooshi)* (2000p) pour du lait\nDe bourbison *(fluffalo)* pour de la fibre de plante\nDe bourbison de feu (2500p) pour des cœurs calcinés *(scorched core)*\nDe bourbison de poison (2500p) pour des échantillons de venin *(venom sample)*\nDe bourbison de glace (2500p) pour des extraits cryogéniques\nDe bourbison électrique (2500p) pour des cellules statiques.",
			  "Des graines à jeter sur de la terre labourée et arrosée : de gazon (10p) ou de gazon fleuri (20p).\nAinsi qu’une petite balle rouge (100p) et une niche *(pet house)* (300p) pour votre animal de compagnie, dans votre vaisseau.",
			  "*Articles liés : Avant-Poste, livraisons Terrafour*"
		  ],
		  "Téléboutique 2-arrêts": [
			  "https://starbounder.org/mediawiki/images/b/bb/2_Stop_Teleshop.gif\nLa __Téléboutique 2-arrêts__ *(2 Stop Teleshop)* est un magasin de l’__Avant-Poste__ spécialisé dans les téléporteurs. On peut y acheter un cœur de téléporteur pour 15 diamants, ou des téléporteurs de formes diverses (mais fonctionnellement identiques) pour un cœur de téléporteur et 3 000 pixels.\nElle n’est ouverte qu’après avoir fini la __mission__ du *Complexe de minage d’Erchius*.",
			  "*Articles liés : téléporteur, pixels*"
		  ],
		  "Mobilier Frögg": [
			  "https://starbounder.org/mediawiki/images/5/5d/Fr%C3%B6gg_Furnishing.gif\n**Mobilier Frögg** *(Frögg Furnishing)* est un magasin de meubles à l’__Avant-Poste__. Il n’ouvre qu’après avoir fini la __mission__ du *Complexe de minage d’Erchius*. Ce magasin a certains objets accessibles en permanence, et des « ensembles » qui changent toutes les 24 h (réelles). On peut changer l’ensemble actuel en changeant l’heure de l’ordinateur.",
			  "**Disponible à tout moment :** codex « Un guide de construction de colonie » *(A Guide To Colony Construction)*, acte de __colonie__ *(colony deed)*, et l’ensemble « extérieur » *(outdoor)*.\n**Ensemble 1 : astronaute** (différentes planètes, lit en navette spatiale…)\n**Ensemble 2 : cabane** (chaise à bascule, vieux poêle trophée de chasse…)\n**Ensemble 3 : cuisine** *(kitchen)* (panier de pommes, huche à pain, plaques de cuisson…)\n**Ensemble 4 : gothique** (trop dark, idéal pour une maison de vampire)\n**Ensemble 5 : île** (hamac, figurine hula…)\n**Ensemble 6 : rétro** (atompunk)\n**Ensemble 7 : pastel** (rose et bleu fluo)\n**Ensemble 8 : mer** *(sea)* (miroir de corail, lampe en coquillages, etc)\n**Ensemble 9 : sp00ky** (lampe citrouille, chaudron…)\n**Ensemble 10 : steampunk**\nPlus de détails ici : https://starbounder.org/Frögg_Furnishing",
			  "*Articles liés : Avant-Poste, colonies*"
		  ],
		  CHEVALET: [
			 " https://starbounder.org/mediawiki/images/e/e4/EASEL.gif\nLe **CHEVALET**, Centre Harmonieux d’Écriteaux Véritables en Automatique Lithographie Élégante et Travaillée *(EASEL, Elegantly Automatic Sign Engineering Lithographer)* est un magasin de l’__Avant-Poste__ permettant de concevoir des panneaux (gratuitement). Il ouvre en finissant la __mission__ *Allons chercher l’artefact florane*.",
			  "Le cabinet à côté a un emplacement *input* (entrée) et huit emplacement *output* (sortie). https://starbounder.org/mediawiki/images/a/a4/SignTut-Frame3.png",
			  "En parlant à l’Hylotl, une interface s’ouvrira vous permettant de créer votre panneau : https://starbounder.org/mediawiki/images/6/62/SignTut-Frame5.png\nLa zone centrale est la zone de dessin, faisant 32×8 pixels. Les flèches permettent de déplacer le dessin. En cliquant sur la bordure, vous pouvez la modifier.\nEn dessous, le bouton *scan input* vous permet de charger le panneau que vous aurez placé dans l’emplacement *input* du cabinet. Les chiffres indiquent les images du panneau, ce dernier pouvant être animé (à 3,3 images/s). Le **+** et le **-** permettent d’ajouter ou supprimer une image, et les flèches de passer à l’image précédente/suivante.\nÀ gauche, *new* (nouveau) permet de créer un nouveau panneau, *clear* (nettoyer) efface complètement l’image courante, *print* (imprimer) ajoute un exemplaire de ce panneau dans un des emplacement *outut* libres, et *undo* (annuler) permet d’annuler la dernière modification.\nÀ droite, *color* permet de choisir la couleur de dessin, *backing* (arrière) permet de changer le fond. Pour les quatre boutons : le premier permet de rendre le panneau connectable (voir __mode câblage_₎. L’ampoule permet de rendre l’image courante éclairée ; l’ampoule prendra la couleur courante et la lumière sera de cette même couleur — et comme cela est spécifique à l’image, vous pouvez faire clignoter ou grésiller votre panneau, et faire changer la lumière de couleur. La pipette permet de sélectionner une couleur depuis le dessin et le seau d’effectuer un remplissage, comme dans un éditeur d’images classique.",
			  "Notez que plusieurs panneaux côte-à-côte fusionneront, aussi bien horizontalement que verticalement. https://starbounder.org/mediawiki/images/3/3c/WantedSigns2.gif",
			  "*Articles liés : Avant-Poste*"
		  ],
		  "Ursa Mineur": [
			  "L’**Ursa Mineur** (*Ursa Miner*, avec *Ursa Minor*, la Petite Ourse) est un magasin de l’__Avant-Poste__ vendant des  minerais raffinés. Il n’est ouvert qu’après avoir fini la __quête__ *l’ourson perdu*, et agrandit son stock à mesure que l’on avance dans l’histoire.",
			  "Son inventaire initial est\n**Charbon** (20p), **lingot de cuivre** (200p), **lingot d’argent** (300p), **lingot d’or** (400p), **lingot de fer** (400p), **pioche de cuivre** (500p).\nEn terminant la __mission__ du *Complexe de minage d’Erchius*, il ajoute des **barre de tungten** (600p) et des **pioches en argent** (750p).\nEn terminant *Allons chercher l’artefact florane*, des **lingots de titanium** (800p), **pioches en or** (1000p) et **foreuses de cuivre** (1500p).\nEn terminant *Allons chercher l’artefact hylotl* : **lingot de duracier** (1000p), **pioche de platine** (1250p), **foreuse en argent** (2250p).\nEn terminant *Allons chercher l’artefact avien* : **égissel raffiné** (1200p), **ferozium raffiné** (1200p), **violium raffiné** (1200p), **pioche en diamant** (1500p), **foreuse en or** (3000p).\nEn terminant *Allons chercher l’artefact apex* : **étoile de solarium** (600p), **foreuse en platine** (3750p).\nEnfin, finir *Allons chercher l’artefact glitch* ajoute la **foreuse en diamant** (4500p).",
			  "*Articles liés : Avant-poste, pioches et foreuses, minerais, pixels*"
		  ],
		  Aisébec: [
			  "L’**Aisébec** *(Beakeasy) est un bar louche de pingouins situé sous l’__Avant-Poste__. On y obtient la __quête__ d’__Orfraie le pingouin__ *(Dreadwing)*, qui une fois complétée fera apparaître des mercenaires pingouins dans les alentours. On peut alors acheter au barman des doublons au prix de 3 diamants, puis donner ces doublons aux mercenaires pingouins pour leur faire rejoindre votre __équipage__ et ainsi obtenir un médiocre soldat ainsi que le succès \"Ami à plumes\" *(Feathered Friend)*.\nÀ côté du bar se trouve __Mazebound64__.",
			  "*Articles liés : Avant-Poste, Orfraie le pingouin, équipage*"
		  ],
		  "Honorable Arsenal de Biggy": [
			  "https://starbounder.org/mediawiki/images/3/31/Biggy%27s_Reputable_Weaponry.gif\nL’**Honorable arsenal de Biggy** *(Biggy's Reputable Weaponry)* est un magasin trouvé sous l’__Avant-Poste__. Il apparaît une fois que vous avez fini la __mission__ *Allons cherche l’artefact hylotl*, mais ce n’ouvrira vraiment qu’en terminant la mission secondaire du *Site d’essais d’armes des pingouins*.",
			  "Il permet d’acheter des __armes__ uniques améliorées en échange de leurs composants, exactement de la même manière que vous le feriez vous-même avec une __enclume__, à la différence que vous n’avez pas besoin d’en connaître les plans *(blueprint)*. Cependant, il n’offrira que deux armes différentes à la fois, qui changent toutes les 24 h (réelles).\n Les armes pouvant s’y trouver sont : __gant à griffes__, __gant tazer__, __gant à lianes__, __boomerang gelé__, __boomerang en fusion, __lunarang__, __chakram durci__, __neo chakram__ et __chakram scie__.",
			  "*Articles liés : Avant-Poste, armes*"
		  ],
		  "Baie des Pingouins": [
			  "https://starbounder.org/mediawiki/images/b/b2/Penguin_Bay.gif\nLa **Baie des Pingouins** *(penguin bay)* est un magasin trouvé à l’__Avant-Poste__, débloqué en finissant la __mission__ *Allons chercher l’artefact florane*. Il permet d’obtenir des licences contrefaites pour agrandir votre __vaisseau__ sans avoir à recruter des membres d’__équipage__. La première licence coûte 10 000 __pixels__, le seconde 20 000, puis 40 000, 80 000 et enfin 150 000.",
			  "*Articles liés : Avant-Poste, vaisseau, Pierre Pingouin*"
		  ],
		  "Pierre Pingouin": [
			  "https://starbounder.org/mediawiki/images/0/09/Shipyard_Captain.png\n**Pierre Pingouin** *(Penguin Pete)* est un vendeur permettant d’acheter des véhicules. Il est également le __PNJ__ vous donnant la __mission__ du *Complexe de Minage d’Erchius*, ainsi que les __quêtes__ pour améliorer votre __vaisseau__.",
			  "Il vend des puces auto *(auto chip)* (100p) qui servent à réparer vos véhicules auprès de __Rob Réparo__. Il vend également des navires (25000p), et des hoverbikes kaki (11000p), rouges (12500p) et verts (15000p). La couleur est purement cosmétique.",
			  "*Articles liés : Avant-Poste, Baie des Pingouins, vaisseau, véhicules, Rob Réparo*"
		  ],
		  "Rob Réparo": [
			  "https://starbounder.org/mediawiki/images/2/20/Rob_Repairo.gif\n**Rob Réparo** est un magasin de l’__Avant-Poste__ situé entre la __Baie des Pingouins__ et __Pierre Pingouin__. Il permet, en échange de puces auto *(auto chip)*, de réparer vos __véhicules__. Chaque puce répare le véhicule d’un cinquième du total, et sont vendues par Pierre Pingouin.",
			  "*Articles liés : Avant-Poste, véhicules, Pierre Pingouin*"
		  ],

		  "l'Arche": [
			  "https://i56.servimg.com/u/f56/17/10/54/90/arche11.png\nL’**Arche** *(Ark*) est une structure qui se trouve à l’est de l’__Avant-Poste__. Un téléporteur se trouvant entre les deux permet d’y accéder plus rapidement. À côté de ce téléporteur, la boutique __Précieux Trophées__ *(Treasured Trophies) apparaîtra après avoir terminé la mission de la __Grande pagode-bibliothèque__. En son centre, une grande porte monolothique est entourée d’une statue du __Cultivateur__, d’une de __la Ruine__, et d’une série d’hologrammes illustrant le combat qui a eu lieu entre les deux.",
			  "La porte est décorée de chaque côté d’un bas-relief représentant les visages des sept espèces jouables, une huitième étant manquante. Autour de la porte, une inscription en __alphabet ancien__ dit « Rassemblez les six clefs et placez-les ici pour ouvrir le trou noir » *(« Collect the six keys and place them here to open the black hole »)*. Ces clés sont les anciens artefacts, qui sont placés dans leurs emplacmeents au centre de la porte à mesure que vous les récupérez durant les __missions__ de l’histoire principale. Une fois les six encastrées dans la porte, celle-ci s’ouvre et révèle un portail menant à la Ruine.",
			  "*Articles liés : Avant-Poste, le Cultivateur, la Ruine, missions*"
		  ],


		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /********************************************************************* MONSTRES **********************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  parasprite: [
			  "https://starbounder.org/mediawiki/images/c/c0/Parasprite.png\nLe **parasprite** est un monstre générique ajouté en 1.3.0. On ne peut en trouvé que dans les vaisseaux abandonnés de l’USCM dans les __anomalies spatiales__. Il y a un __codex__ de l’USCM, *Rapport annuel de l’USCM*, qui donne le contexte du développement du parasprite en tant qu’arme biologique.",
			  "*Monstre générique #59. 50 PV, 8 points de dégâts.\nButin : __griffe tranchante__ (20%), __figurine__ (1%)*",
			  "Il fut créé par les scientifiques de l’USCM et du __Miniknog__. Le Miniknog, qui avait rarement rencontré les dirigeants de l’USCM en face-à-face, considéraient l’USCM comme dangereux. Il a décidé de signer un accord avec l’USCM ; il l’aiderait à développer une arme biologique. Cependant, il a secrètement conçu l’arme pour qu’elle attaque l’USCM au bon moment pour détruire à jamais cette organisation. C’est ainsi que l’USCM fut dissout. Il reste quelques parasprites dans leurs cellules de confinement à bord des vaisseaux abandonnés de l’USCM, et peuvent donc être rencontrés dans le jeu.",
			  "*Articles liés : monstres, USCM, Miniknog*"
		  ],


		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /*********************************************************************** BOSS ************************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  "Asra Nox": [
			  "https://starbounder.org/mediawiki/images/1/1e/Asra_Nox_Boss.gif\n**Asra Nox** est la dirigeante du __culte Occasus__, et entièrement dédiée à l’extermination de toute forme de vie intelligente non __humaine__ de l’univers, et de toute personne voulant l’en empêcher.",
			  "*Boss de la mission « Grande pagode-bibliothèque »\n750 PV. 1 phase. Butin : __Katana Solus__ (10%), __figurine__ d’Asra Nox (5%)",
			  "Rendue orpheline par une bande de bandits aliens quand elle était petite, elle fut recueillie par __Esther Bright__, une __Protectrice__, qui la mis sous la garde du Protectorat, où elle devint la petite protégée d’Esther. Asra était une élève brillante et passionnée, mais les sombres cicatrices de son enfance étaient profondes.",
			  "Quand ses recherches révélèrent l’existence de __la Ruine__, une force de destruction pure des temps oubliés, Esther demanda conseil à Asra dans l’espoir de trouver un moyen d’empêcher à jamais la Ruine de s’échapper de sa prison. Au lieu de ça, Asra vit en la Ruine un moyen de « purifier » l’univers des espèces « inférieures », pour ne laisser que l’humanité. Esther ne put la dissuader et Asra finit par s’enfuir, fondant le culte Occasus avec d’autre humain·e·s qui partageaient ses délires de génocide galactique.",
			  "Asra a en réalité une faible estime de son propre culte, les considérant comme une majorité d’abrutis et de pions jetables. Mais comme elle le dit, « mieux vaut une armée de débiles que pas d’armée du tout ». Et cette armée compte des centaines, voire des milliers de ces débiles tout ce qu’il y a de plus motivé·e·s.",
			  "**     __Rencontres__**",
			  "**  __Grande pagode-bibliothèque__**\nLa première rencontre avec Asra se fait durant la mission de la *Grande pagode-bibliothèque*, où elle et ses brutes recherchent l’artefact __hylotl__. Elle se trouve dans une grande salle au niveau le plus profond et délabré, avec deux grandes étagères servant de plateformes.\nAsra y est protégée par un bouclier d’énergie indestructible durant la majeure partie du combat, obligeant lia joueur·se à esquiver continuellement en attendant une ouverture. Asra exécute une série d’attaques prédéterminée, la rendant prévisible.\nIdéalement, vous serez équipé·e d’une arme avec de forts DPS (Dégâts Par Seconde) pour prendre avantage de ses courtes fenêtres de vulnérabilité, comme un __lance-roquettes__ ou une lance avec *tourbillon* ou *déluge*.",
			  "**Attaques**\n1. Explosion de kunaï : Asra saute haut et lâche un éventail de kunaï qui explosent au contact du sol. L’endroit le plus sûr pour éviter cette attaque est en hauteur, par exemple sur une des étagères ou sur un mur grâce à la __sphère à pics__.\n2. Ruée Solus : Elle se téléporte sur une des étagères puis fonce vers l’autre côté de la pièce, vous infligeant des dégâts si vous êtes sur le passage. Juste après, elle se téléporte dans un coin de fonce vers le coin opposé.\n3. Taillade Solus : Une fois sa ruée terminée, Asra restera au sol un moment. Si vous approchez, elle vous donnera un coup de katana et se téléportera ailleurs pour recommencer. Sinon, elle passe à l’étape suivante.\n4. Sphère à tête chercheuse : Asra se transforme en __sphère de distorsion__ et vous foncera dessus à plusieurs reprises. Après l’avoir fait huit fois, Asra tombe à court d’énergie et doit récupérer, désactivant son bouclier. **C’est sont seul moment vulnérable,** dont profitez-en pour lui mettre tout ce que vous avez de plus puissant.\n5. Rayon laser : après avoir récupéré son énergie, Asra se téléportera dans un des coins inférieurs de l’arène et relâchera un puissant rayon à travers la pièce. Mettez-vous simplement sur une étagère pour l’esquiver.\nAprès cela, elle fera une Taillade Solus, puis recommencera son cycle à 1.\nUne fois vaincue, elle jurera de se venger et s’enfuira.",
			  "**   __Le donjon du baron__**\nAsra ne combat pas directement et se contentera de piloter son énorme __dragon squelette__. Une fois la bête vaincue, Asra s’enfuira à nouveau, abandonnant ses cultistes sur place.",
			  "**   __La Ruine__**\nAsra parvient on ne sait comment à se retrouver au cœur de la Ruine avant lia joueur·se, et provoque un nouveau duel, semblable à celui de la Grande Pagode, à ceci près que son cycle d’attaques n’est constitué que d’une Taillade Solus et de la Sphère à tête chercheuse.\nUne fois vaincue, elle s’enfuit à nouveau, vous laissant vous occuper du cœur de la Ruine, mais contrairement au combat dans la Grande Pagode, elle ne laisse aucun butin.",
			  "**   _Anectodes_**\nSon plat préféré est la __meringue au citrœil__. Elle aime donner des ordres, les grosses armes, et le violet. Elle déteste le __raisin__ et les gens qui marchent trop lentement.",
			  "*Articles liés : Esther Bright, culte Occasus, la Ruine*"
		  ],

		  "la Ruine": [
			  "https://starbounder.org/mediawiki/images/thumb/5/56/RuinLookingAtYou.jpg/320px-RuinLookingAtYou.jpg\nLa **Ruine** est le boss final du jeu, trouvé au bout de la quête *Ouverture de l’Arche*. La vaincre termine l’histoire principale.",
			  "*Boss de la mission « Ouverture de l’Arche »\n5 000 PV. Butin : aucun*",
			  "Une fois que vous traversez le portail de l’Arche nouvellement ouvert et arrivez sur la surface de la Ruine, vous recevrez le message radio de la part de __S.A.I.L.__ suivant : « Les relevés indique que cette structure, bien que semblable à une planète, est en réalité un gigantesque organisme vivant. »",
			  "La Ruine ne trouve là où est normalement le cœur de la planète, trouvée en creusant tout au fond, et située dans l’habituelle arène de boss. La Ruine dans son ensemble est plus planète de taille très petite, avec une circonférence d’environ 1000 blocs. S’y installer est déconseiller car il s’agit d’une instance de mission qui sera regénérée chaque fois que vous y retournerez.",
			  "La Ruine est composée d’amas de tentacules *(tentacle cluster)*, de blobs en gelée *(jelly blob)* et d’obsidienne, d’arbres tentacules et d’abondants lacs de poison. Elle est infestée de divers monstres rampants et volants avec de grands yeux, généralement plus faibles que des monstres de rang 6. Contrairement aux autres missions, les barres de nourriture continuent de se vider pendant que vous êtes sur la Ruine.",
			  "Alors que vous approchez le fond de la couche extérieure, S.A.I.L. envoie un autre message : « Les relevés indiquent la présence d’un vaste gouffre d’une grande profondeur sous vos pieds. Je vous recommande d’essayer de ne pas chuter et mourir. » Malheureusement, le seul moyen d’atteindre l’arène de la Ruine est d’aller au fond du gouffre. Le fond est fait de blocs de cervelle *(brain blocks)* qui sont extrêmement durs à détruire. En approchant la chambre du boss, __Esther Bright__ vous envoie un message radio : « Vous avez atteint le cœur de la Ruine. Vous devez le détruire. »",
			  "**     __Combat__**\nEn premier lieu, vous aurez un nouveau duel contre __Asra Nox__. Puis la Ruine s’éveille.",
			  "**Attaques**\nÉruption : l’œil du Cœur charge un laser puis le tire sous lui (au centre de l’arène), infligeant de lourds dégâts dans un large cône. La zone d’effet est visible quelques secondes au préalable. Il est important de noter que cette attaque touche aussi les ennemis invoqués par la déchirure spatiale.\nVrille tentaculaire : la Ruine enfonce ses tentacules à travers le côté droit ou gauche (ou les deux) de l’arène. Elle ne peut pas effectuer cette attaque en même temps de l’éruption. Si vous êtes touché·e par cette attaque, elle vous repoussera vers le centre de l’arène.\nDéchirure spatiale : un grand portail vers un certain __biome__ s’ouvrira au-dessus de l’œil de la Ruine, et de nombreux petits portails projectiles en seront tirés. Quand ces petits portails touchent le sol ou un mur, ils invoquent un __monstre__ spécifique au biome montré dans le grand portail.",
			  "**Stratégie**\nCe boss a une grande hitbox qui ne vous fait pas de dégâts au contact, donc si vous avez un moyen durable de l’atteindre, n’hésitez pas à aller au corps-à-corps. L’__Asuterosaberu DX__, la __Lame de lia Protecteurice__ et le __bâton de Kluex__ sont très efficaces contre la Ruine.\nMême si les attaques de la Ruines font beaucoup de dégâts, elles sont facilement esquivables en s’en éloignant. Une bonne partie des dégâts que vous recevrez viendront des monstres invoqués, que vous pouvez tenter de rassembler sous l’œil  pour les faire oblitérer par une éruption. Le *blink dash* et le multi-sauts sont très utiles pour ce combat.",
			  "*Articles liés : Asra Nox, culte Occasus, Protectorat Terrene, le Cultivateur*"
		  ],


		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		  /********************************************************************** DIVERS ***********************************************************************/
		  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		  Mazebound64: [
			  "https://starbounder.org/mediawiki/images/b/b3/Mazebound64.gif\n**Mazebound64** est un jeu d’arcade trouvé sous l’__Avant-Poste__. Il s’agit d’un labyrinthe en 3D à la première personne, dont le but est bien sûr d’en sortir. Il y a des statistiques affichées sur le côté mais elles sont purement cosmétiques et ne changent jamais.\nSolution : ||https://starbounder.org/mediawiki/images/thumb/d/de/Mazebound_Map.png/500px-Mazebound_Map.png||",
			  "Finir le jeu vous permettra d’obtenir un succès ainsi qu’un ticket gagnant, qui n’a aucune utilité autrement qu’en tant que trophée. https://starbounder.org/mediawiki/images/6/65/Winning_Ticket.png",
			  "*Articles liés : Avant-Poste, succès*"
		  ]
}),
	  pages = Object.getOwnPropertyNames(wiki), messageListePages = `Mon wiki contient actuellement ${pages.length} pages : ${pages.join(", ")}`;

const { splitMessage } = require("./splitMessage");


// All those freezes actually improve lookup performance
for(const page in wiki)
	Object.freeze(wiki[page]);


function searchSimilar(text, list, limit)
{
	const matches = [];

	text = text.toLowerCase();

	for(const element in list)
	{
		const distance = levenshtein(element.toLowerCase(), text);

		if(distance === 0)
			return [element];
		else if(distance <= limit)
			matches.push(element);
	}

	return matches;
}

module.exports.sendPage = function(channel, request)
{
	const send = channel.send.bind(channel);
	if(request)
	{
		const page = wiki[request];

		if(page)
			for(const partie of page)
				send(partie);
		else
		{
			const alias = searchSimilar(request, aliases, 3);

			if(alias.length === 1)
				this.sendPage(channel, aliases[alias[0]]);
			else
			{
				const alike = searchSimilar(request, wiki, Math.min(5, request.length - 1));

				if(alike.length === 1)
					this.sendPage(channel, alike[0]);
				else if(alike.length)
					send(`Cette page n’existe pas, mais des pages similaires ont été trouvées : ${alike.join(", ")}.`);
				else
					send("Cette page n'existe pas.");
			}
		}
	}
	else
		splitMessage(messageListePages).forEach(send);
}
