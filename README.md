# S.A.I.L.

Version actuelle : 1.2

**Avertissement :** ce bot est fait pour ne fonctionner que sur **un serveur à la fois.**

## Présentation
S.A.I.L. sert à gérer à ma place les rôles de races.

## Utilisation
Ce bot utilise le paquet `discord.js`.

Pour l'utiliser, créez un fichier `auth.json` dans le même dossier que `bot.js` avec les données suivantes :

```JSON
{
	"token": "token d'identification du bot",
 	"master": "votre id utilisateur",
	"roles": {
		"human": "id du rôle pour les humain·e·s",
		"apex": "id du rôle pour les apex",
		"avian": "etc",
		"floran": "",
		"hylotl": "",
		"glitch": "",
		"novakid": "",
		"groovy": "id des joueur·se·s de Wargroove"
	}
}
```

Bien sûr, il faut l'ajouter au serveur. Pensez à mettre son rôle au-dessus de tous les autres, sinon il ne pourra pas modifier les rôles des membres (même s'il est admin).

Plus qu'à lancer la commande `node bot.js` et c'est parti.


## Licence
S.A.I.L. est publié sous licence GNU GPL 3.0.
