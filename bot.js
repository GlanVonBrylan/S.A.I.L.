"use strict";

const Discord = require("discord.js");
const INTENTS = Discord.GatewayIntentBits;
const auth = require("./auth.json");
const wiki = require("./wiki.js");
const client = new Discord.Client({
	intents: [
		INTENTS.Guilds,
		INTENTS.GuildMessages, INTENTS.MessageContent,
		INTENTS.DirectMessages,
	],
	partials: [Discord.Partials.Channel], // for DMs
	presence: auth.support ? {activities: [{name: ">aide | >support"}] , status: "online"} : {status: "online"},
});

const roles = {
	initialised: false,
	human: null,
	apex: null,
	avian: null,
	floran: null,
	hylotl: null,
	glitch: null,
	novakid: null,
	groovy: null
};

var master, myself, ship;

const voyelles = ['a', 'à', 'â', 'ã', 'ä', 'æ', 'e', 'é', 'è', 'ê', 'ë', 'œ', 'i', 'ì', 'î', 'ï', 'o', 'ô', 'õ', 'ö', 'u', 'ù', 'û', 'ü', 'y', 'ÿ', 'h'];



function exit()
{
	client.destroy();
	process.exit();
}

function checkMaster(user)
{
	if(!user.tag) user = user.user;
	return user.id === auth.master;
}


client.on("ready", async () => {
	console.log(`Logged in as ${client.user.tag} !`);
	ship = client.guilds.cache.first();
	master = await client.users.fetch(auth.master);
	myself = await ship.members.fetch(client.user.id);

	if(!roles.initialised)
	{
		for(const race in roles)
			if(race !== "initialised")
				roles[race] = ship.roles.cache.get(auth.roles[race]);

		roles.initialised = true;
		Object.freeze(roles);
	}
});


function error(err)
{
	if(!err.message.includes("ECONNRESET"))
	{
		let msg = "Une erreur est survenue ; lisez la console pour plus de détails.";

		if(err.message)
		{
			if(err.name === "DiscordAPIError")
				msg += `\nMessage : ${err.message}\nChemin : ${err.path}`;
			else
				msg += `\nMessage : ${err.message}.`;
		}

		master.send(msg).catch(console.error);
        console.error(err);
	}
}

client.on("error", error);
process.on("unhandledRejection", error);



client.on("messageCreate", msg => {
	const content = msg.content;
	const cmd = content.split(" ", 1)[0].toLowerCase();
	const prm = content.length > (cmd.length + 1) ? content.substr(cmd.length + 1) : "";
	const srv = msg.guild;

	if(!srv && cmd !== "$help" && cmd !== "$aide" && cmd !== "$shutdown"
	   || msg.system || msg.webhookID || msg.author.bot)
		return;


	if(cmd[0] !== "$")
		return;

	const send = msg.channel.send.bind(msg.channel);

	switch(cmd)
	{
		case "$description":
		case "$presentation":
		case "$présentation":
			send({ embeds: [{
				color: 0x1166DD,
				title: "Ship-based Artificial Intelligence Lattice",
				description: "S.A.I.L. est l'intelligence artificielle en charge du bon fonctionnement des vaisseaux spatiaux, gérant l'équipage, la réserve de carburant, et peut également servir d'agenda pour garder trace de vos missions, expéditions, et rendez-vous chez le dentiste. Cette instance de S.A.I.L., en charge du cuirassé de classe Condor *le Starbünd*, est une version modifiée dans laquelle la gestion de missions est désactivée, parce que nik lé rajeu.\n\nPour plus d'informations sur mes fonctionnalités, tapez la commande `$aide`.",
				fields: [
					{ name: "Code source", value: "https://framagit.org/GlanVonBrylan/S.A.I.L." }
				],
				footer: {
					text: "Créé par " + master.username,
					icon_url: master.avatarURL()
				}
			}]});
			break;

		case "$help":
		case "$aide": {
			const embeds = [{
				color: 0x1166DD,
				title: "Commandes",
				description: "——————————————————————————————————",
				fields: [
					{ name: "$wiki [page]", value: `Affiche la page de wiki indiquée. Si aucune page n'est indiquée, affiche toutes les pages disponibles.` },
					{ name: "$humain·e", value: `Vous donne ou vous retire le rôle ${roles.human}.\nFonctionne aussi avec \`$humain\` et \`$humaine\`.` },
					{ name: "$apex", value: `Vous donne ou vous retire le rôle ${roles.apex}.` },
					{ name: "$avien·ne", value: `Vous donne ou vous retire le rôle ${roles.avian}.\nFonctionne aussi avec \`$avien\` et \`$avienne\`.` },
					{ name: "$florane", value: `Vous donne ou vous retire le rôle ${roles.floran}.\nFonctionne aussi avec \`$floréane\`.` },
					{ name: "$hylotl", value: `Vous donne ou vous retire le rôle ${roles.hylotl}.` },
					{ name: "$glitch", value: `Vous donne ou vous retire le rôle ${roles.glitch}.` },
					{ name: "$novakid", value: `Vous donne ou vous retire le rôle ${roles.novakid}.` },
					{ name: "$groovy", value: `Vous donne ou vous retire le rôle ${roles.groovy}.` }
				],
				footer: {
					text: "Créé par " + master.username,
					icon_url: master.avatarURL()
				}
			}];

			if(checkMaster(msg.author))
				embeds.push({
					color: 0xBBFFBB,
					title: "Commandes rien que pour vous, maître Morgân :",
					description: "——————————————————————————————————",
					fields: [
						{ name: "$eval <code>", value: "Exécute le code JavaScript donné. Attention à ne pas tout casser !" },
						{ name: "$shutdown", value: "Me désactive." }
					]
				});

			send({embeds});
			break;
		}


			/*********************************************************************************************************/
			///////////////////////////////////////////// COMMANDES PERSO /////////////////////////////////////////////
			/*********************************************************************************************************/

		case "$eval":
			if(checkMaster(msg.author))
				eval(prm);
			break;

		case "$shutdown":
			if(checkMaster(msg.author))
				send("Shutting down...").finally(client.destroy.bind(client));
			break;



			/*********************************************************************************************************/
			/////////////////////////////////////////// COMMANDES PUBLIQUES ///////////////////////////////////////////
			/*********************************************************************************************************/

		case "$wiki":
			wiki.sendPage(msg.channel, prm.trim());
			break;


		case "$human":
		case "$humain":
		case "$humaine":
		case "$humain·e":
		case "$humain•e":
		case "$humain-e":
			toggleRole(msg, roles.human);
			break;

		case "$apex":
			toggleRole(msg, roles.apex);
			break;

		case "$avian":
		case "$avien":
		case "$avienne":
		case "$avien·ne":
		case "$avien•ne":
		case "$avien-ne":
			toggleRole(msg, roles.avian);
			break;

		case "$floran":
		case "$florane":
		case "$floreane":
		case "$floréane":
			toggleRole(msg, roles.floran);
			break;

		case "$hylotl":
			toggleRole(msg, roles.hylotl);
			break;

		case "$glitch":
			toggleRole(msg, roles.glitch);
			break;

		case "$novakid":
			toggleRole(msg, roles.novakid);
			break;

		case "$groove":
		case "$wargroove":
		case "$groovy":
			toggleRole(msg, roles.groovy);
			break;


		default:
			if(cmd[0] === "$")
				send("Commande inconnue. Essayez `$aide`.");
			break;
	}
});


function confirmSuccess(message)
{
	message.react("✅").catch(Function());
}

function toggleRole(msg, role)
{
	if(msg.member.roles.cache.has(role.id))
		msg.member.roles.remove(role).then(() => confirmSuccess(msg), error);
	else
		msg.member.roles.add(role).then(() => confirmSuccess(msg), error);
}


client.login(auth.token);
